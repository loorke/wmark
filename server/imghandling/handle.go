package imghandling

import (
	"bytes"
	"image"
	jpeg "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"

	"github.com/noelyahan/mergi"
)

func Handle(img io.Reader, wm io.Reader) ([]byte, error) {
	imgBytes, err := ioutil.ReadAll(img)
	if err != nil {
		return []byte{}, err
	}
	wmBytes, err := ioutil.ReadAll(wm)
	if err != nil {
		return []byte{}, err
	}

	src, _, err := image.Decode(bytes.NewReader(imgBytes))
	if err != nil {
		return []byte{}, err
	}
	watermark, _, err := image.Decode(bytes.NewReader(wmBytes))
	if err != nil {
		return []byte{}, err
	}

	p := image.Pt(10000, 10000)
	resultImg, err := mergi.Watermark(watermark, src, p)
	if err != nil {
		return []byte{}, err
	}
	b := &bytes.Buffer{}
	err = jpeg.Encode(b, resultImg, nil)
	if err != nil {
		return []byte{}, err
	}

	return b.Bytes(), nil
}
