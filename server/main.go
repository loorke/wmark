package main

import (
	"fmt"
	"io"
	"net/http"
	"strconv"

	"./imghandling"
)

func main() {
	server := http.Server{
		Addr:    ":8880",
		Handler: &handler{},
	}

	server.ListenAndServe()
}

type handler struct{}

func (*handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if err := recover(); err != nil {
			e := err.(error)
			fmt.Println(e)
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, "An error occured: "+e.Error())
		}
	}()

	fmt.Println(r.URL)

	if r.URL.String() != "/watermark" {
		io.WriteString(w, "Invalid server method: "+r.URL.String())
		return
	}

	if r.Method != "POST" {
		io.WriteString(w, "/watermark server method supports only POST HTTP requests")
		return
	}

	fmt.Println("<")

	imgFile, _, err := r.FormFile("img")
	if err != nil {
		panic(err)
	}

	wmFile, _, err := r.FormFile("wm")
	if err != nil {
		panic(err)
	}

	fmt.Println(">")

	fmt.Println("<")

	// img, err := ioutil.ReadAll(imgFile)
	// if err != nil {
	// 	panic(err)
	// }

	// wm, err := ioutil.ReadAll(wmFile)
	// if err != nil {
	// 	panic(err)
	// }

	fmt.Println(">")

	result, err := imghandling.Handle(imgFile, wmFile)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(result)))
	if _, err := w.Write(result); err != nil {
		panic(err)
	}
}
