package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"time"
)

func main() {
	imgFile, err := os.Open("img.jpg")
	if err != nil {
		panic(err)
	}
	wmFile, err := os.Open("wm.png")
	if err != nil {
		panic(err)
	}

	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	fmt.Println(imgFile.Name())
	fmt.Println(wmFile.Name())

	imgWriter, err := w.CreateFormFile("img", imgFile.Name())
	if err != nil {
		panic(err)
	}
	_, err = io.Copy(imgWriter, imgFile)
	if err != nil {
		panic(err)
	}

	wmWriter, err := w.CreateFormFile("wm", wmFile.Name())
	if err != nil {
		panic(err)
	}
	_, err = io.Copy(wmWriter, wmFile)
	if err != nil {
		panic(err)
	}

	ct := w.FormDataContentType()
	w.Close()

	req, err := http.NewRequest("POST", "http://dev.example.com:8880/watermark", &b)
	req.Header.Set("Content-Type", ct)

	var client http.Client
	var resp *http.Response
	if resp, err = client.Do(req); err != nil {
		panic(err)
	} else if resp.StatusCode != http.StatusOK {
		panic(errors.New("Status code isn't 200"))
	}

	resultFile, err := os.Create("results/" + time.Now().String() + ".jpg")
	if err != nil {
		panic(err)
	}
	defer resultFile.Close()
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	resultFile.Write(body)

	fmt.Println("DONE!")
}
